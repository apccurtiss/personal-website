const pluginRss = require("@11ty/eleventy-plugin-rss");
const filterDate = require('nunjucks-date-filter');
const markdown_it = require("markdown-it");
const markdown_it_footnote = require("markdown-it-footnote");

module.exports = (config) => {
  config.addPlugin(pluginRss);
  config.addFilter('date', filterDate);

  let markdown_it_plugin =  markdown_it({
    html: true, // Enable HTML tags in source
    // breaks: true, // Convert '\n' in paragraphs into <br>
    linkify: true // Autoconvert URL-like text to links
  }).use(markdown_it_footnote);

  config.setLibrary("md", markdown_it_plugin);

  config.addPassthroughCopy({ 'src/assets': 'assets' });
  // There is a bug where addPassthroughCopy will prevent watching of any files in the ** glob path
  // regardless of their extension.
  // config.addPassthroughCopy('src/posts/**/*.jpg');
  // config.addPassthroughCopy('src/posts/_drafts/canon_t3i_repair/*.png');

  config.setBrowserSyncConfig({
    files: ['dist/**/*'],
    open: true
  })
  config.setDataDeepMerge(true);

  return {
    dir: {
      input: 'src',
      output: 'dist'
    }
  }
}
