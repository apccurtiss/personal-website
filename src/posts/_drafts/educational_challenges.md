---
layout: layouts/base.njk
tags: post
title: Academia is Awful
date: 2024-04-05
eleventyExcludeFromCollections: true
---

## Academia is Awful

There is a common framework for educators called <a href="https://en.wikipedia.org/wiki/Bloom%27s_taxonomy#Cognitive_domain_(knowledge-based)">Bloom's Taxonomy</a>, which tries to classify degrees of understanding.

<img src="/assets/images/blooms_taxonomy.png" width=50%>
