---
eleventyExcludeFromCollections: true
---

Modern Web Development
======================

Prologue: Humble Beginnings
---------------------------

Modern web development is a confusing web of ever-changing technologies with blurred boundaries and complex dependencies. This guide details, to the best of my knowledge, exactly what each of these tools actually does. It comes from the perspective of the very basics of web development- starting from hand-written HTML, CSS, and JavaScript, it asks exactly what improvements each technology brings and exactly how it is implemented.

Act I: NodeJS and NPM
---------------------

In the before times, JavaScript could only be used in the browser. Then scientists ripped it out and stuffed it's heart into `NodeJS`, which is just JavaScript but it can run outside of the browser like a normal language. It no longer has a DOM or mouse events, but now it can access files on your computer and do everything else a language normally does. It even has a package manager called `npm`! You will be using it a lot. It does have competition on the form of `Bower` and `Yarn`, but don't start with either. So now everything can be JavaScript! All the cool kids are doing it. Nearly every tool in modern web development is written this way, and requires `NodeJS` to run, so it's a requirement for the modern development stack. Together, `NodeJS` and `npm` are at the core of modern web development. They have three main uses:

1. Installing third-party packages  
   Rather than manually downloading third-party JavaScript and CSS, you can use `npm` for actual versioned package management. Installing a package with `npm install [package]` does three things:
    - It creates a folder called `node_modules/` in the current directory and stores the code there.
    - It also creates a file called `package.json` that stores a list of each package you have downloaded, as well as some optional versioning and build information. More on that later.
    - It also creates a third file called `package-lock.json` which contains not only your dependencies, but the exact version of every sub-dependency they have as well. This one is purely optional and you can safely delete it, but it's helpful so there's really no reason to.
2. Installing and running build tools
   You can also use `npm` to install build tools (written for `NodeJS`) that aren't bound to a specific project by adding the flag `npm install -g [package]`, which will skip the local `node_modules/` folder and instead make the package globally accessible. This will often add new commands to your command line.

   _Aside: There's nothing from preventing you from installing regular libraries that are not global build tools with the `-g` flag, and vice versa, but you're just causing problems if you try._

3. Creating build scripts  
   In addition to it's normal package manager duties, `npm` can also be used to run build steps by adding to the `scripts` dictionary in `package.json` (remember, the file created by `npm install`?). You can add arbitrary build scripts there, then run them with `npm run [script name]`. Most sites will use this to document and run their development and production builds.

Act II: Simple Compilers
-------------------------

The most difficult part of building a website is the user base, who will all be viewing it in a browser that _only_ supports HTML, CSS, and JavaScript. If you want to use anything else, you have to convert it to these base languages first with some sort of compiler.

HTML Compilers
--------------

Nunjucks
--------

Nunjucks extends HTML with useful modern language features, such as importing other HTML files, using loops to repeat individual code blocks, and even some basic conditional logic.

Example: A page with the "navigation" tag that has a link to every page with a "post" tag:

```html
---
tags: ['navigation']
---

{% for post in collections.post %}
     <a href="{{ post.url }}">
         {{ post.data.title }}
     </a>
{% endfor %}
```

_Aside: This compilation is purely HTML-based, and there is no easy way to build something like a JavaScript-powered tab menu._

Markdown
--------

Compiles a small subset of common HTML features like headers and links using human-readable source files.

Example: A contact page with a header, a bulleted list, and links.

```markdown
Contact
=======

- [Twitter](https://twitter.com/me)
- [LinkedIn](https://linkedin.com/in/me)
```

Haml
----

Completely re-imagines HTML syntax and adds support for Ruby templates.

Example: A simple date display powered by Ruby.

```haml
%h1 Today's date is:

%b= Time.now.inspect
```

Jekyll
------

Similar HTML compilers that add fun add-ons like for loops and variables, then compile them away into valid HTML. They are entire frameworks though, and require building your site around them. They are highly opinionated. You should use `Nunjucks` instead.

CSS Compilers
-------------



JavaScript Compilers
--------------------

Babel
-----

`Babel` is a JavaScript to JavaScript compiler. It's only real built-in functionality is simply to convert modern code to older versions for compatibility reasons. However, it offers a powerful plugin system which lets you add your own extensions to JavaScript, which will be compiled away into something valid. Like most things, `Babel` can run client- or server-side. On the server, where it should be run, it turns source code into regular production-ready JavaScript. If you're ridiculous, you can run it on the client by including the `Babel` script, then giving your other scripts `type="text/babel"` or something similar, rather than `type="text/javascript"`. These scripts wouldn't run normally, but `Babel` will compile and run them.

Typescript
----------

A superset of JavaScript with types. There's really no reason not to use it.

JSX
---

Lets you write inline script tags into your JavaScript, like `var x = <h1 class="banner">Hello</h1>`. This will be compiled into something resembling `var x = document.createElement('h1'); d.classList.add('banner'); d.textContent = 'Hello'`. While helpful, this will impact the overall code size and rendering speed. It also greatly increases the complexity for human- and machine- readability of an app.

LiquidJS
--------

An HTML template language that's like Nunjucks, but is capable of running client-side at render time. Because of this, it isn't suppose to render entire files and isn't as feature-rich.

Pug
---

Another HTML template language. Meant to be compiled server-side into a JavaScript function that can be passed client-side, which will return HTML.

Act III: Bundlers and Task Runners
---------------------------------

_"But wait"_, you cry, _"how do I actually include these packages in my HTML?"_

Let me tell you a story.

Long ago, if you wanted to combine two JavaScript files you had to write successive `<script>` tags, so the code in the first tag would run and make its global variables accessible to the later `<script>` tags.

This led to much weeping and gnashing of teeth.

Then `CommonJS` came along, and designed a compiler for JavaScript files. It functioned thusly: if a file declared a variable called `exports`, another file could load that variable with the function `require('file/path')`. These would then be compiled into a single file which could be included in a `<script>` tag like normal. Thus scoped modules were born.

Then `NodeJS` looked upon what `CommonJS` had made, and saw that it was good, but decided to tweak it a little anyway just to fuck with people. And thus `NodeJS` used the same syntax but with the `exports` variable replaced with `module.exports`. And because it was built in, no compiling step was needed.

Then the JavaScript community collectively decided that maybe `NodeJS` was on to something here, and reinvented it for a third time. Now individual variables could be declared as `export foo = 1`, and imported using `import {foo, bar} from 'file'`. And because it was now built into the browser, it still did not require a compilation step. It did require [this](https://stackoverflow.com/a/53821485) though.

Act IV: JavaScript Frameworks
------------------------------

Despite being popular for years,

React
-----

`React` is a client-side library that makes it easy to integrate your HTML and JavaScript. Because it is purely a JavaScript library, it can be included with a single `<script>` tag or `import` statement. However, writing the HTML in `React` would be a massive pain without the use of `JSX syntax`, a second (optional) library which lets you write the HTML directly in with your JavaScript. Adding this syntax to JavaScript requires using a tool like `Babel` as a preprocessing step before the JavaScript is run, which can be done both server- or client-side. You can use `JSX` syntax without `React`.

At the core of `React` are Components: each Component is a JavaScript class that provides all the regular benefits of classes, such as code reuse and encapsulation, but can also be rendered to HTML and will be re-rendered automatically when its internal state changes. A `React` app will be built out of nested Components, rather than as a single homogeneous blob of HTML. This makes writing UIs much easier, but comes at the cost of app bloat. The React framework _must_ be included client-side, requiring a large but cachable download when a page is first loaded. See `JSX` for additional performance concerns.

Aside: `React` started life as a web framework, but has since expanded to mobile development. This has led to a split between the `React` library, which contains the core Component logic, and the `ReactDOM` library, which contains web-specific logic. This separation is not perfect in practice, and will likely change over time.

```html
React.createElement('div', {className: 'shopping-list'},
```

Preact
------

`Preact` is `React` but smaller and faster. It drops a couple minor features but is otherwise identical.

Angular
-------

A very complex, full-fledged framework. It has a similar component system to `React`, but has an entire server-side build architecture to support it. To be honest, this is as far as I understand it because the documentation is so dense.

Vue
---

Like `React`, `Vue` is a front-end framework that can be imported with a single `<script>` tag. While `React` tries to integrate HTML directly into JavaScript, `Vue` is based off of writing the HTML separately, then passing it to the `Vue` constructor, which will transform the HTML as necessary and set it up to be re-rendered reactively. [TODO: Can also undergo server-side transformations, what are those?]

Svelte
------

`Svelte` is a newer framework that relies on a client-side compile step. It converts HTML and JavaScript to a pure JavaScript bundle and adds reactivity through narrowly-focused code that updates only the parts of the DOM that are modified by a state change, rather than re-rendering the whole component.

Button example:

```html
<script>
	let count = 0;

	function handleClick() {
		count += 1;
	}
</script>

<button on:click={handleClick}>
	Clicked {count} {count === 1 ? 'time' : 'times'}
</button>
```

Compiles to:

```html
<script>
  class App extends SvelteComponent {
    // A bunch of incomprehensible compiled JavaScript such as:
    t3 = text(t3_value);
    p(ctx, [dirty]) {
    	if (dirty & /*count*/ 1) set_data(t1, /*count*/ ctx[0]);
    	if (dirty & /*count*/ 1 && t3_value !== (t3_value = (/*count*/ ctx[0] === 1 ? "time" : "times") + "")) set_data(t3, t3_value);
    }
  }

  const app = new App({
  	target: document.body,
  });
</script>
```

Appendix: JavaScript libraries and Other Helpers
------------------------------------------------

JQuery
------

A set of generic JavaScript helper functions, such as the ability to use `$('.foo')` rather than `document.getElementsByClassName('foo')`. It gets less and less popular as time goes on, but an annoying large number of front-end libraries still depend on it.

Mustache.js
-----------

A dead-simple template engine that injects values into double-braced variables like `my name is {{name}}`. Works on HTML or strings.

Handlebars.js
-------------

Mustache.js with slightly more functionality, like nested objects and custom plugins.