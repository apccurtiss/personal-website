---
layout: layouts/base.njk
tags: post
title: Security Career Advice
date: 2024-04-05
eleventyExcludeFromCollections: true
---

## Security Career Advice

Here's a smattering of advice I often give to anyone who wants to start their security career.

_(Do you have a job in computer security? Let me know which of these tips worked for you and which didn't!)_

### Build your network

The single most important thing you can do is **start making connections with other people**. Right now! Find a security meetup like [BoulderSec](https://www.bouldersec.org/) and plan to attend their next meeting. Are you a student? Join a club like the [CyberSecurity Club](https://cucyberclub.com/) and make some friends. Get to know the people in your classes. Find a teacher who is holding office hours right now and go ask them about their research.

Academia has a nasty habit of teaching students that grades and raw programming talent are the only skills they need to succeed. **This is not true in the real world**. Most of your professional opportunities will come from other people: you want contacts who tell you about job postings you wouldn't know about, and they  You certainly need the skills for whatever job you are applying for, but just as importantly: **you need people**.

### Get real-world experience



### Start broad

Asdf
