(function() {
    modal = document.getElementById("x-modal");

    modal.onclick = hideModal;

    function hideModal() {
        modal.classList.add("hidden");
    }
    
    function displayInModal(element) {
        modal.replaceChildren(element);
        modal.classList.remove("hidden");
    }

    Array.from(document.getElementsByClassName("x-modal-on-click")).forEach(function (element) {
        element.onclick = function() {
            clone = element.cloneNode(true);
            clone.classList.remove("x-modal-on-click");
            displayInModal(clone);
        }
    })
})()